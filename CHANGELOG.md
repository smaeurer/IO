0.7.3
-----
- Added unit tests
- Fixed reg ex for invalid path characters
- Fixed directory empty property on deleting

0.7.2
-----
- Fixed current directory in fileinfo class

0.7.1
-----
- Changed to gitflow